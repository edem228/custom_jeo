  <div class="map-container col-md-8">
  	<div id="map_<?php echo jeo_get_map_id(); ?>" class="map"></div>
  	<?php if(is_single()) : ?>
  		<?php if(jeo_has_marker_location()) : ?>
  			<div class="highlight-point transition has-end" data-end="1300"></div>
  		<?php endif; ?>
  	<?php endif; ?>
  	<?php do_action('jeo_map'); ?>
  </div>
  <div class="col-md-4">
    <?php
    // get the lasters post
      $args = array( 'numberposts' => '1' );
      $recent_posts = wp_get_recent_posts( $args );

      foreach( $recent_posts as $recent ){
        echo '<div class="post-image">'.the_post_thumbnail( array(460, 300)).'</div>';
        echo '<h3><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a></h3>';
        echo the_excerpt();
        echo '<aside class="actions clearfix"><a href="<?php the_permalink(); ?>">'. "Read more" .'</a>';
      }
    ?>
  </div>
<script type="text/javascript">jeo(<?php echo jeo_map_conf(); ?>);</script>