<?php if(have_posts()) : ?>
    <section class="posts-section">
        <div class="container">
            <ul class="posts-list">
                <?php while(have_posts()) : the_post(); ?>
                    <li id="post-<?php the_ID(); ?>" <?php post_class('six columns'); ?>>
                        <article id="post-<?php the_ID(); ?>" class="clearfix">
                            <div class="col-sm-5" style="margin-top: 20px;">
                                <div class="post-image"> <?php the_post_thumbnail( array(200, 200)); ?></div>
                            </div>
                            <header class="post-header">
                                <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                            </header>
                            <section class="post-content">

                                <div class="post-excerpt">
                                    <?php the_excerpt(); ?>

                                </div>
                            </section>

                        </article>
                    </li>
                <?php endwhile; ?>
            </ul>
            <div class="twelve columns">
                <div class="navigation">
                    <?php posts_nav_link(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>


