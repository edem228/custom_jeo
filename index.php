<?php get_header(); ?>


<?php masterslider(2); ?>

<div class="section-title">
	<div class="container">
		<div class="twelve columns">
			<h2><?php _e('Special Projects', 'jeo'); ?></H2>
		</div>
	</div>
</div>
<?php get_template_part('loop'); ?>

<?php get_footer(); ?>